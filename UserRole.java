import javax.swing.*;

public class UserRole{

	public static void main(String[] args){

	String input = new String(JOptionPane.showInputDialog("Please enter the user role that specifies your clearance level."));
	String role = new String(input.toLowerCase());

	switch(role){
		case "administrator":
			JOptionPane.showMessageDialog(null, "Welcome, " + role.toUpperCase() + "! Your clearance is level 1.");
		break;

		case "faculty":
			JOptionPane.showMessageDialog(null, "Welcome, " + role.toUpperCase() + "! Your clearance is level 2.");
		break;

		case "staff":
			JOptionPane.showMessageDialog(null, "Welcome, " + role.toUpperCase() + "! Your clearance is level 3.");
		break;

		case "student":
			JOptionPane.showMessageDialog(null, "Welcome, " + role.toUpperCase() + "! Your clearance is level 4.");
		break;

		case "guest":
			JOptionPane.showMessageDialog(null, "Welcome, " + role.toUpperCase() + "! Your clearance is level 5.");
		break;

		default:
			JOptionPane.showMessageDialog(null, "ERROR: Unable to match user role to clearance level.");
		}
	}
}
